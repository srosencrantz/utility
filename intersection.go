package utility

import (
	"fmt"
)

// Intersection describes the where a ray and a facet intersect
type Intersection struct {
	Obj   fmt.Stringer // pointer to the thing hit
	Front bool         // did the ray hit the front (positive normal) side of the facet
	Dist  float64      // distance of the intersection from the root point of the ray
	Loc   [3]float64   // location in 3d of the inersection
}

func (x *Intersection) String() string {
	return fmt.Sprintf("intersection: %v\n", *x) //fmt.Sprintf("Object: %s\nFront: %v\nDist: %f\nLoc %v\n\n", x.Obj.String(), x.Front, x.Dist, x.Loc)
}

// IntersectionList is a list of intersections that can be sorted from smallest Distance to largest
type IntersectionList []*Intersection

func (a IntersectionList) String() string {
	var result string
	for i, x := range a {
		result += fmt.Sprintf("%d: \n", i) + x.String()
	}
	return result
}

func (a IntersectionList) Len() int           { return len(a) }
func (a IntersectionList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a IntersectionList) Less(i, j int) bool { return a[i].Dist < a[j].Dist }
