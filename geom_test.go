/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package utility

import (
	"testing"
)

func TestTriIntersect(t *testing.T) {
	p1 := [3]float64{1.0, 1.0, -1.0}
	p2 := [3]float64{1.0, -1.0, -1.0}
	p3 := [3]float64{1.0, 0.0, 1.0}
	rayOrigin := [3]float64{0.0, 0.0, 0.0}
	rayEnd := [3]float64{2.0, 0.0, 0.0}

	intersect, loc, err := TriIntersect(p1, p2, p3, rayOrigin, rayEnd)
	if !intersect {
		t.Errorf("TriIntersect said the Ray didn't intersect the triangle and it should have.")
	}

	if loc != 0.5 {
		t.Errorf("TriIntersect got t = %f, expected t = 0.5", loc)
	}

	if err != nil {
		t.Errorf("TriIntersect got err: %s expected none.", err)
	}

	intersect2, front, distance, point := TriIntersectComprehensive(p1, p2, p3, rayOrigin, rayEnd)
	if !intersect2 {
		t.Errorf("TriIntersectComprehensive said the Ray didn't intersect the triangle and it should have.")
	}

	if distance != 1.0 {
		t.Errorf("TriIntersectComprehensive got distance = %f, expected distance = 1.0", distance)
	}

	if !front {
		t.Errorf("TriIntersectComprehensive said it hit the back it actually hit the front")
	}

	if point != [3]float64{1.0, 0.0, 0.0} {
		t.Errorf("TriintersectComprehensive got intersection point %v it should be %v", point, [3]float64{1.0, 0.0, 0.0})
	}
}

func triSamplePoints(a1, a2, a3 [3]float64, area float64, maxDiv int) (aSamplePoints [][3]float64) {
	//fmt.Printf("triangle = [%5.6f %5.6f %5.6f;%5.6f %5.6f %5.6f;%5.6f %5.6f %5.6f]\n",
	//	a1[0], a1[1], a1[2], a2[0], a2[1], a2[2], a3[0], a3[1], a3[2])
	aSamplePoints = TriSamplePoints(a1, a2, a3, area, maxDiv)
	//fmt.Printf("samplepoints = [")
	//for _, point := range aSamplePoints {
	//	fmt.Printf("%5.6f %5.6f %5.6f;", point[0], point[1], point[2])
	//}
	//fmt.Printf("]\n")
	return aSamplePoints
}

func TestTriSamplePoints(t *testing.T) {
	area := 1.0
	maxDiv := 10

	//Facet, 928964, 1900, 100, 1041, 443149, 443151, 443150
	a1 := [3]float64{-138.303192138671875, 55.0151519775390625, 75.86029815673828125}
	a3 := [3]float64{-124.30319976806640625, 55.749988555908203125, 75.75046539306640625}
	a2 := [3]float64{-138.303192138671875, 54.749988555908203125, 75.75046539306640625}
	aSamplePoints := triSamplePoints(a1, a2, a3, area, maxDiv)
	if len(aSamplePoints) != 4 {
		t.Errorf("TestTriSamplePoints got %d it should be %d", len(aSamplePoints), 4)
	}

	//Facet, 929954, 1900, 100, 1041, 443190, 443358, 443357
	b1 := [3]float64{-143.2880401611328125, -53.50000762939453125, 89.8603057861328125}
	b2 := [3]float64{-143.1782073974609375, 49.499988555908203125, 90.12546539306640625}
	b3 := [3]float64{-143.2880401611328125, 49.499988555908203125, 89.8603057861328125}
	bSamplePoints := triSamplePoints(b1, b2, b3, area, maxDiv)
	if len(bSamplePoints) != 16 {
		t.Errorf("TestTriSamplePoints got %d it should be %d", len(bSamplePoints), 16)
	}

	c1 := [3]float64{0, 0, 0}
	c2 := [3]float64{3, 0, 0}
	c3 := [3]float64{1.5, 3, 0}
	cSamplePoints := triSamplePoints(c1, c2, c3, area, maxDiv)
	if len(cSamplePoints) != 16 {
		t.Errorf("TestTriSamplePoints got %d it should be %d", len(cSamplePoints), 16)
	}
}

func TestTriArea(t *testing.T) {
	a1 := [3]float64{-138.303192138671875, 55.0151519775390625, 75.86029815673828125}
	a3 := [3]float64{-124.30319976806640625, 55.749988555908203125, 75.75046539306640625}
	a2 := [3]float64{-138.303192138671875, 54.749988555908203125, 75.75046539306640625}
	area := TriArea(a1, a2, a3)
	tol := 0.000001
	ans := 2.009820972349
	if area > ans+tol || area < ans-tol {
		t.Errorf("TriArea got %f it should be %f", area, ans)
	}
}
