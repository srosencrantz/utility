/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package utility

import (
	"math"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/vect3d"
)

func TriNormal(p1, p2, p3 [3]float64) (normal [3]float64) {
	return vect3d.Normalize(vect3d.CrossProduct(vect3d.Subtract(p2, p1), vect3d.Subtract(p3, p1)))
}

// TriIntersectComprehensive runs TriIntersect and returns more information:
//   whether the front (positive normal side) was hit, the distance of the intersection from the origin, and the intersection point
func TriIntersectComprehensive(p1, p2, p3, rayOrigin, rayEnd [3]float64) (intersect bool, front bool, distance float64, point [3]float64) {
	intersect, t, err := TriIntersect(p1, p2, p3, rayOrigin, rayEnd)
	if err != nil || !intersect {
		return intersect, front, distance, point
	}
	rvect := vect3d.Subtract(rayEnd, rayOrigin)
	eot := vect3d.ScalerMult(rvect, t)
	distance = vect3d.Magnitude(eot)
	point = vect3d.Add(rayOrigin, eot)
	normal := vect3d.CrossProduct(vect3d.Subtract(p2, p1), vect3d.Subtract(p3, p1))
	front = false
	if vect3d.GetAngle(rvect, normal) < 0 {
		front = true
	}
	return intersect, front, distance, point
}

// TriIntersect determines whether a line segment defined by rayOrigin and rayEnd intersects a triangle defined by the points p1, p2, p3. This algorithm was originally from an efficient ray-polygon intersection by Didier Badouel from the book Graphics Gems I */
func TriIntersect(p1, p2, p3, rayOrigin, rayEnd [3]float64) (intersect bool, t float64, err error) {

	n := vect3d.CrossProduct(vect3d.Subtract(p2, p1), vect3d.Subtract(p3, p1))
	d := vect3d.DotProduct(vect3d.ScalerMult(p1, -1.0), n)
	rayDir := vect3d.Subtract(rayEnd, rayOrigin)
	nDotRayDir := vect3d.DotProduct(n, rayDir)

	// check if is ray parallel to the triangle plane
	if nDotRayDir == 0.0 {
		return false, 0, errcat.AppendInfoStr(err, "ray is parallel to triangle plane")
	}

	t = -1.0 * ((d + vect3d.DotProduct(n, rayOrigin)) / nDotRayDir)

	// check if intersection is beyond the ends of the line segment
	if (t <= 0.0) || (t >= 1.0) {
		return false, t, errcat.AppendInfoStr(err, "ray is beyond the ends of the line segment")
	}

	nmax := MaxLoc([]float64{math.Abs(n[0]), math.Abs(n[1]), math.Abs(n[2])})
	i1, i2 := int(0), int(0)
	if nmax == 0 {
		i1 = 1
		i2 = 2
	} else if nmax == 1 {
		i1 = 0
		i2 = 2
	} else {
		i1 = 0
		i2 = 1
	}

	p := vect3d.Add(rayOrigin, vect3d.ScalerMult(rayDir, t))
	u := [3]float64{p[i1] - p1[i1], p2[i1] - p1[i1], p3[i1] - p1[i1]}
	v := [3]float64{p[i2] - p1[i2], p2[i2] - p1[i2], p3[i2] - p1[i2]}

	if u[1] == 0.0 {
		beta := u[0] / u[2]
		if (beta >= 0.0) && (beta <= 1.0) {
			alpha := (v[0] - beta*v[2]) / v[1]
			intersect = (alpha >= 0.0) && ((alpha + beta) <= 1.0)
			return intersect, t, err
		}
	} else {
		beta := (v[0]*u[1] - u[0]*v[1]) / (v[2]*u[1] - u[2]*v[1])
		if (beta >= 0.0) && (beta <= 1.0) {
			alpha := (u[0] - beta*u[2]) / u[1]
			intersect = (alpha >= 0.0) && ((alpha + beta) <= 1.0)
			return intersect, t, err
		}
	}
	return false, t, err
}

// MaxLoc determines the position of the largest element in the array
func MaxLoc(array []float64) (maxloc int) {
	maxloc = 0
	for i := 1; i < len(array); i++ {
		if array[i] > array[maxloc] {
			maxloc = i
		}
	}
	return maxloc
}

// contants for the GeoDesicPoints function
const x float64 = 0.525731112119133606
const z float64 = 0.850650808352039932

var vdata = [12][3]float64{
	{-x, 0.0, z}, {x, 0.0, z}, {-x, 0.0, -z}, {x, 0.0, -z},
	{0.0, z, x}, {0.0, z, -x}, {0.0, -z, x}, {0.0, -z, -x},
	{z, x, 0.0}, {-z, x, 0.0}, {z, -x, 0.0}, {-z, -x, 0.0}}
var tindices = [20][3]int{
	{0, 4, 1}, {0, 9, 4}, {9, 5, 4}, {4, 5, 8}, {4, 8, 1},
	{8, 10, 1}, {8, 3, 10}, {5, 3, 8}, {5, 2, 3}, {2, 7, 3},
	{7, 10, 3}, {7, 6, 10}, {7, 11, 6}, {11, 0, 6}, {0, 1, 6},
	{6, 1, 10}, {9, 0, 11}, {9, 11, 2}, {9, 2, 5}, {7, 2, 11}}

// GeoDesicPoints returns a list of points equally distributed over a sphere
// depth 0 returns 12 points, 1 returns 72 points, 2 returns 312 points, 3 returns 1272 points
// radius sets the radius of the points about the origin
func GeoDesicPoints(depth int, radius float64) (spherePoints [][3]float64) {
	spherePoints = make([][3]float64, 0)
	spherePoints = append(spherePoints, vdata[:]...)
	for i := 0; i < 20; i++ {
		geoSubdivide(vdata[tindices[i][0]], vdata[tindices[i][1]], vdata[tindices[i][2]], &spherePoints, depth)
	}
	for i := range spherePoints {
		spherePoints[i] = vect3d.ScalerMult(spherePoints[i], radius)
	}
	return spherePoints
}

func geoSubdivide(v1, v2, v3 [3]float64, spherePoints *[][3]float64, depth int) {
	if depth == 0 {
		return
	}
	v12 := vect3d.Normalize(vect3d.Add(v1, v2))
	v23 := vect3d.Normalize(vect3d.Add(v2, v3))
	v31 := vect3d.Normalize(vect3d.Add(v3, v1))
	*spherePoints = append(*spherePoints, [][3]float64{v12, v23, v31}...)
	geoSubdivide(v1, v12, v31, spherePoints, depth-1)
	geoSubdivide(v2, v23, v12, spherePoints, depth-1)
	geoSubdivide(v3, v31, v23, spherePoints, depth-1)
	geoSubdivide(v12, v23, v31, spherePoints, depth-1)
}

// TriSamplePointsNew creates a list of sample points on a triangle
func TriSamplePointsNew(v1, v2, v3 [3]float64, min float64) (samplePoints [][3]float64) {
	samplePoints = make([][3]float64, 0)

	// move the 3 points in from the verticies just a little bit so their not right on the edge
	center := vect3d.ScalerMult(vect3d.Add(v3, vect3d.Add(v1, v2)), 0.33333)
	v1b := vect3d.Add(v1, vect3d.ScalerMult(vect3d.Subtract(center, v1), 0.01))
	v2b := vect3d.Add(v2, vect3d.ScalerMult(vect3d.Subtract(center, v2), 0.01))
	v3b := vect3d.Add(v3, vect3d.ScalerMult(vect3d.Subtract(center, v3), 0.01))

	// get some information about the side of the triangle opposite v1
	dir32 := vect3d.Subtract(v3b, v2b)
	len32 := vect3d.Magnitude(dir32)
	dir32 = vect3d.Normalize(dir32)
	numPts32 := int(math.Max(math.Floor(len32/min), 1.0))
	dist32 := len32 / float64(numPts32)

	// going to just use v1 and segment 32 to make the rest of the rays
	samplePoints = append(samplePoints, v1b)

	for i := 0; i < numPts32+1; i++ {
		samplePoints = append(samplePoints, vect3d.Add(v2b, vect3d.ScalerMult(dir32, float64(i)*dist32)))
	}

	for _, point := range samplePoints[1:len(samplePoints)] {
		segDir := vect3d.Subtract(point, v1b)
		segLen := vect3d.Magnitude(segDir)
		segDir = vect3d.Normalize(segDir)
		numPts := int(math.Max(math.Floor(segLen/min), 1.0))
		segDist := segLen / float64(numPts)

		for i := 0; i < numPts; i++ {
			newPt := vect3d.Add(v1b, vect3d.ScalerMult(segDir, float64(i)*segDist))
			keepPt := true
			for _, currPt := range samplePoints {
				if vect3d.DistPt2Pt(newPt, currPt) < min {
					keepPt = false
					break
				}
			}
			if keepPt {
				samplePoints = append(samplePoints, newPt)
			}
		}
	}

	return samplePoints
}

// TriSamplePoints creates a list of sample points on a triangle
func TriSamplePoints(v1, v2, v3 [3]float64, min float64, maxDiv int) (samplePoints [][3]float64) {
	samplePoints = make([][3]float64, 0)
	TriSubdivide(v1, v2, v3, min, maxDiv, 1, &samplePoints)
	return samplePoints
}

// TriSubdivide adds the center point to samplePoints if the maximum edge length is
//  <= min.  other wise it it splits the triangle into 4 triangles tries again.
func TriSubdivide(v1, v2, v3 [3]float64, min float64, maxDiv, currDiv int, samplePoints *[][3]float64) {
	if TriArea(v1, v2, v3) <= min || currDiv == maxDiv {
		*samplePoints = append(*samplePoints, TriMidPt(v1, v2, v3))
		return
	}
	v12 := vect3d.ScalerMult(vect3d.Add(v1, v2), 0.5)
	v23 := vect3d.ScalerMult(vect3d.Add(v2, v3), 0.5)
	v31 := vect3d.ScalerMult(vect3d.Add(v3, v1), 0.5)
	nextDiv := currDiv + 1
	TriSubdivide(v1, v12, v31, min, maxDiv, nextDiv, samplePoints)
	TriSubdivide(v2, v23, v12, min, maxDiv, nextDiv, samplePoints)
	TriSubdivide(v3, v31, v23, min, maxDiv, nextDiv, samplePoints)
	TriSubdivide(v12, v23, v31, min, maxDiv, nextDiv, samplePoints)
}

// TriMaxSize returns the maximum edge length of the facet.
func TriMaxSize(v1, v2, v3 [3]float64) (max float64) {
	return math.Max(math.Max(vect3d.DistPt2Pt(v1, v2), vect3d.DistPt2Pt(v2, v3)), vect3d.DistPt2Pt(v3, v1))
}

// TriMinSize returns the maximum edge length of the facet.
func TriMinSize(v1, v2, v3 [3]float64) (min float64) {
	return math.Min(math.Min(vect3d.DistPt2Pt(v1, v2), vect3d.DistPt2Pt(v2, v3)), vect3d.DistPt2Pt(v3, v1))
}

// TriArea returns the area of a triangle
func TriArea(v1, v2, v3 [3]float64) (area float64) {
	return 0.5 * vect3d.Magnitude(vect3d.CrossProduct(vect3d.Subtract(v2, v1), vect3d.Subtract(v3, v1)))
}

// TriMidPt finds the midpoint of the triangle formed by the given points.
func TriMidPt(v1, v2, v3 [3]float64) (midPt [3]float64) {
	return vect3d.ScalerMult(vect3d.Add(v1, vect3d.Add(v2, v3)), 0.33333333)
}
