/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package utility

import (
	"errors"
)

// LinearInterpolation finds the y value associated with xNew given a list of x and y values using linear interpolation.
func LinearInterpolation(x, y []float64, xNew float64) (float64, error) {
	// this function assumes increasing values of x
	if len(x) != len(y) {
		return float64(0.0), errors.New("utility.LinearInterpolation arrays x and y are not equal length")
	}
	if len(x) == 0 {
		return float64(0.0), errors.New("utility.LinearInterpolation arrays x and y have zero length")
	}
	if xNew < x[0] {
		return float64(0.0), errors.New("utiilty.LinearInterpolation xNew is less than the minimum x value")
	}
	if xNew > x[len(x)-1] {
		return float64(0.0), errors.New("utiilty.LinearInterpolation xNew is greater than the maximum x value")
	}
	for i := 0; i < len(x)-1; i++ {
		if x[i] >= x[i+1] {
			return float64(0.0), errors.New("utility.LinearInterpolation requires increasing values of x")
		}
		if xNew >= x[i] && xNew <= x[i+1] {
			yNew := y[i] + (y[i+1]-y[i])*((xNew-x[i])/(x[i+1]-x[i]))
			return yNew, nil
		}
	}
	return float64(0.0), errors.New("utility.LinearInterpolation could not find x[i] < xNew < x[i+1]")
}
