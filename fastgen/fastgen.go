package fastgen

func GetDensity(mat_id int32) float64 {
	// Density is in units of grains/in^3
	density := [...]float64{1980.0, 1980.0, 1980.0, 1980.0, 1980.0,
		1980.0, 1130.0, 701.0, 672.0, 672.0, 672.0, 686.0, 708.0, 693.0, 430.0,
		1980.0, 1800.0, 2240.0, 2760.0, 4727.0, 188.0, 235.0, 325.0, 295.0, 309.0,
		485.0, 620.0, 240.0, 190.0, 4669.0, 1980.0, 1980.0, 256.0, 329.0, 175.0,
		91.0, 392.0, 380.0, 392.0, 392.0, 392.0, 4214.0, 4214.0, 4214.0, 1141.0,
		0.0, 0.0, 0.0, 0.0, 0.0}

	return density[mat_id-1]
}

func GetBulkModulus(mat_id int32) float64 {
	// Bulk Modulus is in grains/in^3
	bulk_modulus := [...]float64{1.61e11, 1.61e11, 1.61e11, 1.61e11, 1.61e11,
		1.61e11, 1.28e11, 0.70e11, 0.70e11, 0.70e11, 0.70e11, 0.70e11, 0.70e11,
		0.70e11, 0.70e11, 0.29e11, 1.61e11, 1.05e11, 1.33e11, 0.14e11, 1.40e11,
		6.36e09, 5.25e08, 3.20e09, 5.30e09, 5.30e09, 0.16e11, 0.45e11, 0.0, 0.0,
		1.61e11, 1.61e11, 1.61e11, 0.0, 3.50e09, 7.30e09, 1.96e09, 2.00e11, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.34e11, 0.0, 0.0, 0.0, 0.0, 0.0}

	return bulk_modulus[mat_id-1]
}

func GetElasticModulus(mat_id int32) float64 {
	elastic_modulus := [...]float64{2.10e11, 2.10e11, 2.10e11, 2.10e11, 2.10e11,
		2.10e11, 2.10e11, 1.15e11, 0.735e11, 0.721e11, 0.72e11, 0.72e11, 0.70e11,
		0.72e11, 0.72e11, 0.46e11, 2.10e11, 1.58e11, 1.19e11, 0.168e11, 7.63e11,
		0.63e09, 0.22e10, 0.320e10, 0.34e10, 0.21e11, 0.70e11, 0.0, 0.0, 1.68e11,
		2.10e11, 2.10e11, 3.60e11, 3.50e09, 1.3e10, 9.10e09, 3.69453, 0.0, 0.0,
		0.0, 0.0, 1.89e11, 1.89e11, 1.89e11, 1.043e11, 0.0, 0.0, 0.0, 0.0, 0.0}

	return elastic_modulus[mat_id-1]
}

func GetYieldStrength(mat_id int32) float64 {
	// Grains/in^2
	yield_strength := [...]float64{2.20e08, 3.75e08, 5.60e08, 7.40e08, 9.150e08,
		10.95e08, 8.40e08, 3.22e08, 2.31e08, 2.73e08, 2.73e08, 2.80e08, 5.11e08,
		3.50e08, 2.03e08, 10.5e08, 4.20e08, 3.15e08, 0.15e08, 1.96e08, 0.63e08,
		1.33e08, 0.36e08, 0.40e08, 0.47e08, 1.40e08, 0.56e08, 0.0, 0.0, 8.40e08,
		18.00e08, 20.0e08, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}

	return yield_strength[mat_id-1]
}

type CthreatDatabase []*FragmentGroup

type FragmentGroup struct {
	Thl   float64 // The smallest polar angle in the polarzone.
	Thu   float64 // The largest polar angle in the polarzone.
	Xl    float64 // Polarzone lower edge in inches along the warhead
	Xu    float64 // Polarzone upper edge in inches along the warhead
	Angle float64 // thu - thl
	Num   int     // The number of fragments in this group.
	Shape int     // The shape code for the fragment.
	Mass  float64 // The fragment mass in grains (7000 grains in a lb)
	Ld    float64 // ratio of the length to the diameter L/D
	Mat   int     // The fragement fastgen material id.
	Vel   float64 // The fragment velocity in feet per second
	Pa    float64 // The fragment Presented area in inches^2.
}

func (fg *FragmentGroup) YieldStrength() float64 {
	return GetYieldStrength(fg.Mat)
}

func (fg *FragmentGroup) ElasticModulus() float64 {
	return GetElasticModulus(fg.Mat)
}

func (fg *FragmentGroup) BulkModulus() float64 {
	return GetBulkModulus(fg.Mat)
}

func (fg *FragmentGroup) Density() float64 {
	return GetDensity(fg.Mat)
}

func ReadCthreat(filename string) CthreatDatabase {
	var err error
	var f *os.File

	if f, err = os.Open(filename); err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(f)
	cthreat := make(CthreatDatabase, 0, 0)
	polarzonfound := false
	for scanner.Scan() {
		var thl, thu, xl, xu, angle float64
		line := strings.Trimspace(scanner.Text())

		if strings.HasPrefix(line, "POLARZON") {
			polarzonfound = true
			fmt.Sscan(line[17:25], &thl)
			fmt.Sscan(line[25:33], &thu)
			fmt.Sscan(line[33:41], &xl)
			fmt.Sscan(line[41:49], &xu)
			angle = thu - thl

		} else if strings.HasPrefix(line, "FRAGDEF") {
			if !polarzonfound {
				log.Fatal("CTHREAT file did not contain a POLARZON record prior to the first FRAGDEF record")
			}
			fg := new(FragmentGroup)
			fg.Thl = thl
			fg.Thu = thu
			fg.Xl = xl
			fg.Xu = xu
			fg.Angle = angle
			fmt.Sscan(line[9:17], &fg.Num)
			fmt.Sscan(line[17:25], &fg.Shape)
			fmt.Sscan(line[33:41], &fg.Mass)
			fmt.Sscan(line[41:49], &fg.Ld)
			fmt.Sscan(line[49:57], &fg.Vel)
			fmt.Sscan(line[57:65], &fg.Pa)
			cthreat = append(cthreat, fg)
		}
	}
	f.Close()
	return cthreat
}
