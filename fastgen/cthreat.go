package fastgen

import (
	"bitbucket.org/srosencrantz/go_utility/fragcloud"
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func ReadCthreatFile(filename string, size float64) []*fragcloud.Fragment {
	var err error
	var f *os.File

	if f, err = os.Open(filename); err != nil {
		log.Fatal(err)
	}
	fList := make([]*fragcloud.Fragment, 0)
	scanner := bufio.NewScanner(f)

	var pzfound bool
	var pzthl, pzthu, pzlow, pzup, pzang, pztotang float64

	for scanner.Scan() {
		switch {
		case strings.HasPrefix(scanner.Text(), "POLARZON"):
			pzfound = true
			// convert to cth units
			pzthl = strconv.ParseFloat(scanner.Text()[17:25], 64)
			pzthu = strconv.ParseFloat(scanner.Text()[25:33], 64)
			pzlow = strconv.ParseFloat(scanner.Text()[33:41], 64) * 30.48 * (1.0 / 12.0)
			pzup = strconv.ParseFloat(scanner.Text()[41:49], 64) * 30.48 * (1.0 / 12.0)
			pzang = polarzonethuend - polarzonethlstart
			pztotang = polarzoneangle + polarzonetotalangle

		case strings.HasPrefix(scanner.Text(), "FRAGDEF"):
			if !polarzonefound {
				log.Fatal("CTHREAT file did not contain a POLARZON record prior to the first FRAGDEF record.")
			}
			// convert to cth units
			fnum := get_int(line, 9, 17, "NFRAG")
			fshape := get_int(line, 25, 33, "FRAGSH")
			fmass := get_double(line, 17, 25, "FRAGMS") * (1.0 / 7000.0) * 453.592
			fltdia := get_double(line, 33, 41, "FRAGLD")
			fmat := get_int(line, 41, 49, "FRAGMAT")
			fvel := get_double(line, 49, 57, "FRAGSPD") * 30.48 * (1 / 1)
			fpa := get_double(line, 57, 65, "FRAGPA") * 30.48 * 30.48 * (1.0 / 12.0) * (1.0 / 12.0)

			switch {
			case fragmass < minmass:
			case fragvel < minvel:
			case (0.5 * fragmass * fragvel * fragvel) < minke:
				// case fragdens < mindens:
			default:
				for i := 0; i < fnum; i++ {
					switch fshape {
					case 1:
						fList = append(fList, NewCompactCylinderFragment(fmass, fltdia, fmat, fpa, vel))
					case 2:
						fList = append(fList, NewCylinderFragment(fmass, fltdia, fmat, fpa, vel))
					case 3:
						fList = append(fList, NewDiamondFragment(fmass, fltdia, fmat, fpa, vel))
					case 4:
						fList = append(fList, NewSphereFragment(fmass, fltdia, fmat, fpa, vel))
					case 5:
						fList = append(fList, NewCubeFragment(fmass, fltdia, fmat, fpa, vel))
					default:
						fList = append(fList, NewParallelpipedFragment(fmass, fltdia, fmat, fpa, vel))
					}
				}
			}
		}
	}
	f.Close()
	return eList
}
