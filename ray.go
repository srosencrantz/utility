package utility

import (
	"math"

	"bitbucket.org/srosencrantz/vect3d"
)

// Ray defines a ray or a segment of a ray with rootpt and an endpt.
// Distance from the RootPt to the actual root point (to allow for ray splitting)
// if the whole ray is represented by RootPt and EndPt this number would be 0.
type Ray struct {
	RootPt   [3]float64 // The Root point of this ray segment.
	EndPt    [3]float64 // The End point of this ray segment.
	Distance float64
}

// Dir returns the rays direction vector
func (ray *Ray) Dir() (dir [3]float64) {
	return vect3d.Subtract(ray.EndPt, ray.RootPt)
}

// MinLoc returns the min location on the dir axis
func (ray *Ray) MinLoc(dir DirectionType) (min float64) {
	return math.Min(ray.RootPt[dir], ray.EndPt[dir])
}

// MaxLoc returns the max location on the dir axis
func (ray *Ray) MaxLoc(dir DirectionType) (min float64) {
	return math.Max(ray.RootPt[dir], ray.EndPt[dir])
}
