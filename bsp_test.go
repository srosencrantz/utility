package utility

import (
	"fmt"
	"math"
	"testing"
)

type Facet struct {
	ID  int    // Facet Id
	Pid int    // Part ID
	Mid int    // Material ID
	Cid int    // Component ID
	Nid [3]int // Node IDs
	// internal indicies
	Nodes []*Node
}

func (f *Facet) String() (output string) {
	output = fmt.Sprintf("Facet ID: %d \n", f.ID)
	for _, n := range f.Nodes {
		output += fmt.Sprintf("   NID: %d X: %f Y: %f Z: %f\n", n.ID, n.Loc[0], n.Loc[1], n.Loc[2])
	}
	return output
}

func (f *Facet) AvgLoc(dir DirectionType) (avg float64) {
	return (f.Nodes[0].Loc[dir] + f.Nodes[1].Loc[dir] + f.Nodes[2].Loc[dir]) / 3.0
}

func (f *Facet) MinLoc(dir DirectionType) (min float64) {
	return math.Min(math.Min(f.Nodes[0].Loc[dir], f.Nodes[1].Loc[dir]), f.Nodes[2].Loc[dir])
}

func (f *Facet) MaxLoc(dir DirectionType) (max float64) {
	return math.Max(math.Max(f.Nodes[0].Loc[dir], f.Nodes[1].Loc[dir]), f.Nodes[2].Loc[dir])
}

func (f *Facet) Intersect(ray *Ray) (il *Intersection) {
	p1 := f.Nodes[0].Loc
	p2 := f.Nodes[1].Loc
	p3 := f.Nodes[2].Loc
	intersect, front, dist, loc := TriIntersectComprehensive(p1, p2, p3, ray.RootPt, ray.EndPt)
	if !intersect {
		return nil
	}
	return &Intersection{Obj: f, Front: front, Dist: dist + ray.Distance, Loc: loc}
}

//Node describes a node in 3d space.
type Node struct {
	ID  int        // node id
	Loc [3]float64 // node x, y, z
}

var nodes = []*Node{&Node{ID: 1, Loc: [3]float64{78.78124237060546875, -17.828105926513671875, 38.840362548828125}},
	&Node{ID: 2, Loc: [3]float64{76.00118255615234375, -17.828105926513671875, 39.937480926513671875}},
	&Node{ID: 3, Loc: [3]float64{78.39111328125, -18.02933502197265625, 38.399959564208984375}},
	&Node{ID: 4, Loc: [3]float64{74.80915069580078125, -18.02933502197265625, 39.813541412353515625}},
	&Node{ID: 5, Loc: [3]float64{73.2211151123046875, -17.828107833862304688, 41.03460693359375}},
	&Node{ID: 6, Loc: [3]float64{71.22718048095703125, -18.02933502197265625, 41.2271270751953125}},
	&Node{ID: 7, Loc: [3]float64{70.4410552978515625, -17.828107833862304688, 42.13169097900390625}},
	&Node{ID: 8, Loc: [3]float64{67.6452178955078125, -18.02933502197265625, 42.640666961669921875}},
	&Node{ID: 9, Loc: [3]float64{67.66098785400390625, -17.828107833862304688, 43.228816986083984375}},
	&Node{ID: 10, Loc: [3]float64{78.78124237060546875, 17.828046798706054688, 38.840362548828125}},
	&Node{ID: 11, Loc: [3]float64{78.78124237060546875, 21.614101409912109375, 38.840362548828125}},
	&Node{ID: 12, Loc: [3]float64{78.39111328125, 18.02927398681640625, 38.399959564208984375}},
	&Node{ID: 13, Loc: [3]float64{78.39111328125, 21.614101409912109375, 38.399959564208984375}},
	&Node{ID: 14, Loc: [3]float64{78.39111328125, 25.198925018310546875, 38.399959564208984375}},
	&Node{ID: 15, Loc: [3]float64{78.78124237060546875, 25.400152206420898438, 38.840362548828125}},
	&Node{ID: 16, Loc: [3]float64{78.78124237060546875, -25.400213241577148438, 38.840362548828125}},
	&Node{ID: 17, Loc: [3]float64{78.78124237060546875, -21.61415863037109375, 38.840362548828125}},
	&Node{ID: 18, Loc: [3]float64{78.39111328125, -25.198986053466796875, 38.399959564208984375}},
	&Node{ID: 19, Loc: [3]float64{78.39111328125, -21.61415863037109375, 38.399959564208984375}}}

var facets = []*Facet{&Facet{ID: 1, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{1, 2, 3}},
	&Facet{ID: 2, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{3, 2, 4}},
	&Facet{ID: 3, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{4, 2, 5}},
	&Facet{ID: 4, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{6, 5, 7}},
	&Facet{ID: 5, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{8, 7, 9}},
	&Facet{ID: 6, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{8, 6, 7}},
	&Facet{ID: 7, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{4, 5, 6}},
	&Facet{ID: 8, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{10, 11, 12}},
	&Facet{ID: 9, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{12, 11, 13}},
	&Facet{ID: 10, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{13, 11, 14}},
	&Facet{ID: 11, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{14, 11, 15}},
	&Facet{ID: 12, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{16, 17, 18}},
	&Facet{ID: 13, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{18, 17, 19}},
	&Facet{ID: 14, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{19, 17, 3}},
	&Facet{ID: 15, Pid: 4, Mid: 1, Cid: 1, Nid: [3]int{3, 17, 1}}}

func init() {
	for _, f := range facets {
		for _, nid := range f.Nid {
			f.Nodes = append(f.Nodes, nodes[nid-1])
		}
	}
}

func TestCreateBspTree(t *testing.T) {
	tree, err := CreateBspTree(facets)
	if err != nil {
		t.Error(err)
	}
	//fmt.Println(tree.Sprint())

	numObjs := len(tree.Objs)
	expectedObjs := 2
	if numObjs != expectedObjs {
		t.Errorf("TestCreateBspTree got root Objs = %d, expected t = %d", numObjs, expectedObjs)
	}
	numObjs = len(tree.Greater.Objs)
	expectedObjs = 0
	if numObjs != expectedObjs {
		t.Errorf("TestCreateBspTree got root.greater Objs = %d, expected t = %d", numObjs, expectedObjs)
	}
	numObjs = len(tree.Lesser.Objs)
	expectedObjs = 5
	if numObjs != expectedObjs {
		t.Errorf("TestCreateBspTree got root.lesser Objs = %d, expected t = %d", numObjs, expectedObjs)
	}
	numObjs = len(tree.Greater.Greater.Objs)
	expectedObjs = 4
	if numObjs != expectedObjs {
		t.Errorf("TestCreateBspTree got root.greater.greater Objs = %d, expected t = %d", numObjs, expectedObjs)
	}
	numObjs = len(tree.Greater.Lesser.Objs)
	expectedObjs = 4
	if numObjs != expectedObjs {
		t.Errorf("TestCreateBspTree got root.greater.lesser Objs = %d, expected t = %d", numObjs, expectedObjs)
	}
}

func TestFindIntersections(t *testing.T) {
	tree, err := CreateBspTree(facets)
	if err != nil {
		t.Error(err)
	}
	ray := &Ray{RootPt: [3]float64{116.53, 228.15, 24.1}, EndPt: [3]float64{41.03, -182.15, 53.1}, Distance: 0.0}
	intersections := tree.FindIntersections(ray, true, "")
	//fmt.Println("intersections: ")
	//fmt.Println(intersections.String())

	numIntersections := len(intersections)
	expectedIntersections := 2
	if numIntersections != expectedIntersections {
		t.Errorf("TestFindIntersections found %d intersections, expected %d", numIntersections, expectedIntersections)
	}

	firstFacetID := intersections[0].Obj.(*Facet).ID
	expectedID := 10
	if firstFacetID != expectedID {
		t.Errorf("TestFindIntersections found the first intersecton on facet ID = %d, expected ID = %d", firstFacetID, expectedID)
	}

	secondFacetID := intersections[1].Obj.(*Facet).ID
	expectedID = 4
	if secondFacetID != expectedID {
		t.Errorf("TestFindIntersections found the second intersecton on facet ID = %d, expected ID = %d", secondFacetID, expectedID)
	}

	firstFacetDist := intersections[0].Dist
	expectedDist := 209.972505
	if firstFacetDist > (expectedDist+1e-5) && firstFacetDist < (expectedDist-1e-5) {
		t.Errorf("TestFindIntersections found the first intersection at %f, expected %f", firstFacetDist, expectedDist)
	}

	secondFacetDist := intersections[0].Dist
	expectedDist = 250.821913
	if secondFacetDist > (expectedDist+1e-5) && secondFacetDist < (expectedDist-1e-5) {
		t.Errorf("TestFindIntersections found the second intersection at %f, expected %f", secondFacetDist, expectedDist)
	}
}
