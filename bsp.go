package utility

import (
	"fmt"
	"reflect"
	"sort"

	"bitbucket.org/srosencrantz/vect3d"
)

// DirectionType specifies the X, Y, or Z directions
type DirectionType int

// Enums for DirectionType
const (
	X DirectionType = 0
	Y DirectionType = 1
	Z DirectionType = 2
)

// Next gives you the next direction
func (dir DirectionType) next() (nextDir DirectionType) {
	switch dir {
	case X:
		nextDir = Y
	case Y:
		nextDir = Z
	case Z:
		nextDir = X
	}
	return nextDir
}

// BspObj is an interface for an object that can be stored in the bsp tree structure
type BspObj interface {
	String() string
	AvgLoc(DirectionType) float64
	MinLoc(DirectionType) float64
	MaxLoc(DirectionType) float64
	Intersect(*Ray) *Intersection
}

// BspNode represents a node of a BSP of facets
type BspNode struct {
	Objs     []BspObj
	Layer    int
	Dir      DirectionType
	SplitVal float64
	Greater  *BspNode
	Lesser   *BspNode
}

// Sprint prints a tree
func (node *BspNode) Sprint() (output string) {
	nodeList := []*BspNode{node}
	output = ""
	for i := 0; i < len(nodeList); i++ {
		newStr, newNodes := nodeList[i].sprint()
		nodeList = append(nodeList, newNodes...)
		output = fmt.Sprintf("%s%s", output, newStr)
	}
	return output
}

func (node *BspNode) sprint() (output string, bspNL []*BspNode) {
	output = fmt.Sprintf("Layer: %d, Num BspObj: %d, Dir: %v, SplitValue: %f\n", node.Layer, len(node.Objs), node.Dir, node.SplitVal)
	for i, obj := range node.Objs {
		output += fmt.Sprintf("BspObj %d:\n ", i) + obj.String()
	}
	if node.Greater != nil {
		bspNL = append(bspNL, node.Greater)
	}
	if node.Lesser != nil {
		bspNL = append(bspNL, node.Lesser)
	}
	return output, bspNL
}

// CreateBspTree creates a BSP tree given a list of facets
func CreateBspTree(objs interface{}) (node *BspNode, err error) {
	if reflect.TypeOf(objs).Kind() != reflect.Slice {
		return nil, fmt.Errorf("CreateBspTree was not passed a slice")
	}
	s := reflect.ValueOf(objs)
	if s.Len() == 0 {
		return nil, fmt.Errorf("CreateBspTree was passed an empty slice")
	}

	iobjs := make([]BspObj, s.Len())
	var ok bool
	for i := 0; i < s.Len(); i++ {
		iobjs[i], ok = s.Index(i).Interface().(BspObj)
		if !ok {
			return nil, fmt.Errorf("CreateBspTree was passed a slice of items that don't implement the BspObj interface")
		}
	}

	node = &BspNode{Objs: iobjs, Layer: 0, Dir: X}
	nodeList := []*BspNode{node}
	for i := 0; i < len(nodeList); i++ {
		nodeList = append(nodeList, nodeList[i].setup()...)
	}
	return node, nil
}

func (node *BspNode) setup() (bspNL []*BspNode) {
	if len(node.Objs) < 2 {
		return
	}

	// calculate the SplitValue
	for _, obj := range node.Objs {
		node.SplitVal += obj.AvgLoc(node.Dir)
	}
	node.SplitVal /= float64(len(node.Objs))
	// determine which objects go where, if the object is located on:
	//  - both sides of splitVal, then the facet goes in this BspNode's Facets List
	//  - the lesser side of splitVal only, then the facet goes to the Lesser BspNode
	//  - the greater side of splitVal only, then the facet goes to the greater BspNode
	newObjs := make([]BspObj, 0)
	greaterObjs := make([]BspObj, 0)
	lesserObjs := make([]BspObj, 0)
	for _, obj := range node.Objs {
		if obj.MinLoc(node.Dir) < node.SplitVal && obj.MaxLoc(node.Dir) < node.SplitVal {
			lesserObjs = append(lesserObjs, obj)
		} else if obj.MinLoc(node.Dir) > node.SplitVal && obj.MaxLoc(node.Dir) > node.SplitVal {
			greaterObjs = append(greaterObjs, obj)
		} else {
			newObjs = append(newObjs, obj)
		}
	}

	// create the lesser and greater nodes
	node.Objs = newObjs
	if len(lesserObjs) > 0 {
		node.Lesser = &BspNode{Objs: lesserObjs, Dir: node.Dir.next(), Layer: node.Layer + 1}
		bspNL = append(bspNL, node.Lesser)
	}
	if len(greaterObjs) > 0 {
		node.Greater = &BspNode{Objs: greaterObjs, Dir: node.Dir.next(), Layer: node.Layer + 1}
		bspNL = append(bspNL, node.Greater)
	}
	return bspNL
}

type nodeRay struct {
	Node *BspNode
	Ray  *Ray
}

// FindIntersections creates a sorted list of intersections between a ray and the facets in a bsptree if all is true
// if all is false FindIntersection returns when the first intersection is found.
func (node *BspNode) FindIntersections(ray *Ray, all bool, exception string) (intersections IntersectionList) {
	nodeRayList := []*nodeRay{&nodeRay{Node: node, Ray: ray}}
	for i := 0; i < len(nodeRayList); i++ {
		newIntersections, newNodeRays := nodeRayList[i].Node.findIntersections(nodeRayList[i].Ray, all, exception)
		nodeRayList = append(nodeRayList, newNodeRays...)
		intersections = append(intersections, newIntersections...)
		if !all && len(intersections) > 0 {
			return intersections
		}
	}
	sort.Sort(intersections)
	return intersections
}

func (node *BspNode) findIntersections(ray *Ray, all bool, exception string) (intersections IntersectionList, nrl []*nodeRay) {

	// first check for intersections in this node
	if node.Objs != nil {
		for _, obj := range node.Objs {
			newIntersection := obj.Intersect(ray)
			if newIntersection != nil {
				if newIntersection.Obj.String() != exception {
					intersections = append(intersections, newIntersection)
					if !all {
						break
					}
				}
			}
		}
	}

	if node.Lesser == nil && node.Greater == nil {
		return intersections, nil
	}

	switch {
	case ray.MinLoc(node.Dir) < node.SplitVal && ray.MaxLoc(node.Dir) <= node.SplitVal:
		if node.Lesser != nil {
			nrl = append(nrl, &nodeRay{Node: node.Lesser, Ray: ray})
		}

	case ray.MinLoc(node.Dir) >= node.SplitVal && ray.MaxLoc(node.Dir) > node.SplitVal:
		if node.Greater != nil {
			nrl = append(nrl, &nodeRay{Node: node.Greater, Ray: ray})
		}

	default:
		// calc greater and lesser rays
		pvec := [3]float64{0, 0, 0}
		p0 := [3]float64{0, 0, 0}
		pvec[node.Dir] += (1.0 + node.SplitVal)
		p0[node.Dir] += node.SplitVal
		rayDir := vect3d.Subtract(ray.EndPt, ray.RootPt)
		t := vect3d.DotProduct(vect3d.Subtract(p0, ray.RootPt), pvec) / vect3d.DotProduct(rayDir, pvec)
		divPoint := vect3d.Add(ray.RootPt, vect3d.ScalerMult(rayDir, t))
		newDist := ray.Distance + vect3d.DistPt2Pt(ray.RootPt, divPoint)
		var lray, gray *Ray
		if ray.RootPt[node.Dir] <= node.SplitVal {
			lray = &Ray{RootPt: ray.RootPt, EndPt: divPoint, Distance: ray.Distance}
			gray = &Ray{RootPt: divPoint, EndPt: ray.EndPt, Distance: newDist}
		} else {
			lray = &Ray{RootPt: divPoint, EndPt: ray.EndPt, Distance: newDist}
			gray = &Ray{RootPt: ray.RootPt, EndPt: divPoint, Distance: ray.Distance}
		}
		if node.Lesser != nil {
			nrl = append(nrl, &nodeRay{Node: node.Lesser, Ray: lray})
		}
		if node.Greater != nil {
			nrl = append(nrl, &nodeRay{Node: node.Greater, Ray: gray})
		}
	}

	return intersections, nrl
}
