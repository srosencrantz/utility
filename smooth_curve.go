/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package utility

import (
	"errors"
	"math"
)

// Smooth processes a curve and tries to reduce the number of points by eliminating points where significant error is not incured
//  zeroTol is used to determine if a value should actually be zero. If it is less than 0 it will be determined from them min and max y.
//  If zeroEnds is true points with y = 0.0 will be inserted at the begining and end of the resulting lists. It will also ensure
//  that there are not many zeros at the end of the list.
func Smooth(x, y []float64, zeroTol float64, zeroEnds bool) ([]float64, []float64, error) {
	// Check validity of input curve_data
	if len(x) != len(y) {
		return nil, nil, errors.New("utility.Smooth arrays x and y are not equal length")
	}
	if len(x) == 0 {
		return nil, nil, errors.New("utility.Smooth arrays x and y have zero length")
	}

	// Establish a zero tolerance +/- for determining if a value is zero use the provided value if > 0 else calculate one from the min and max
	if zeroTol < 0 {
		avgdelta := float64(0.0)
		for i := 2; i < len(y); i++ {
			yInterp, err := LinearInterpolation([]float64{x[i-2], x[i]}, []float64{y[i-2], y[i]}, x[i-1])
			if err != nil {
				return nil, nil, err
			}
			avgdelta += math.Abs(yInterp - y[i-1])
		}
		zeroTol = (avgdelta / float64(len(y)-2)) * math.Abs(zeroTol)
		//		if zeroTol == 0 {
		//		fmt.Printf("    zeroTol = %g avgdelta = %g div = %g\n", zeroTol, avgdelta, avgdelta/float64(len(y)-2))
		// avgdelta = 0.0
		// for i := 2; i < len(y); i++ {
		// 	yInterp, err := LinearInterpolation([]float64{x[i-2], x[i]}, []float64{y[i-2], y[i]}, x[i-1])
		// 	if err != nil {
		// 		return nil, nil, err
		// 	}
		// 	avgdelta += math.Abs(yInterp - y[i-1])
		// 	fmt.Printf("avgdelta = %g, yInterp = %g, y[i-1] = %g\n", avgdelta, yInterp, y[i-1])
		// }
		//	log.Println(x, y)
		//	log.Fatal("crap")
		//}
	}

	//Create new curve lists
	newX := make([]float64, 0)
	newY := make([]float64, 0)

	// Insert first point in original array assumed to be zero
	if zeroEnds && x[0] != float64(0.0) {
		newX = append(newX, float64(0.0))
		newY = append(newY, float64(0.0))
	} else {
		newX = append(newX, x[0])
		newY = append(newY, y[0])
	}

	// Find first non-zero point in the original array
	last := int64(0)
	if zeroEnds && x[0] != float64(0.0) {
		for last = 0; math.Abs(y[last]) < zeroTol; last++ {
		}

		// Insert a Zero Y value before the first non-zero item
		if last == 0 {
			last = 1
		}
		newX = append(newX, x[last-1])
		newY = append(newY, float64(0))
	}

	// Main smoothing loop over the rest of the data
	for current := last + 2; current < int64(len(x)); current++ {
		for test := last + 1; test < current; test++ {
			yInterp, err := LinearInterpolation([]float64{x[last], x[current]}, []float64{y[last], y[current]}, x[test])
			if err != nil {
				return nil, nil, err
			}
			dist := math.Abs(yInterp - y[test])
			// Store last into the new array when a point is out of tolerance
			if dist > zeroTol {
				last = current - 1
				newX = append(newX, x[last])
				newY = append(newY, y[last])
				break
			}
		}
	}
	// add the last point of the original arrays
	newX = append(newX, x[len(x)-1])
	newY = append(newY, y[len(y)-1])

	if zeroEnds {
		if len(newY) > 0 {
			// This part of the code clips off the last zeros from the set by working backwards
			newlen := len(newY)
			for math.Abs(newY[newlen-1]) < zeroTol {
				newY = newY[0 : newlen-2]
				newX = newX[0 : newlen-2]
				if len(newY) == 0 {
					break
				}
			}
		}

		//  This defines a new points of zero at current time + dt and t=1
		lastID := len(newX) - 1
		newX = append(newX, newX[lastID]+(x[lastID]-x[lastID-1]))
		newY = append(newY, float64(0.0))

		newX = append(newX, float64(5.0)*newX[len(newX)-1])
		newY = append(newY, float64(0.0))
	}
	return newX, newY, nil
}
