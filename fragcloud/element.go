// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	"bitbucket.org/srosencrantz/go_utility/vect3d"
	"math"
)

// Element stores data about the fragment material in a cth element
type Element struct {
	Volm  float64    // volume fraction of the element that is fragment
	Dens  float64    // density of the fragment material in the element
	Size  float64    // length of the cube
	Mass  float64    // mass of the fragment material in the element
	Vol   float64    // volume of the fragment material in the element
	Vel   [3]float64 // velocity of the material in the element
	Nodes []*Node    // coordinates of the corners of the element
}

// CreateElement is used to create a Element specifically calculating the index position and nodal locations
func NewElement(volm, dens, size float64, vel, pos [3]float64) *Element {
	vol := volm * size * size * size
	mass := dens * vol
	nodelist := make([]*Node, 8)
	hs := size / 2.0
	nodelist[0] = &Node{pos[0] + hs, pos[1] - hs, pos[2] - hs}
	nodelist[1] = &Node{pos[0] + hs, pos[1] + hs, pos[2] - hs}
	nodelist[2] = &Node{pos[0] - hs, pos[1] + hs, pos[2] - hs}
	nodelist[3] = &Node{pos[0] - hs, pos[1] - hs, pos[2] - hs}
	nodelist[4] = &Node{pos[0] + hs, pos[1] - hs, pos[2] + hs}
	nodelist[5] = &Node{pos[0] + hs, pos[1] + hs, pos[2] + hs}
	nodelist[6] = &Node{pos[0] - hs, pos[1] + hs, pos[2] + hs}
	nodelist[7] = &Node{pos[0] - hs, pos[1] - hs, pos[2] + hs}
	elem := &Element{volm, dens, size, mass, vol, vel, nodelist}
	return elem
}

// Pos calculates the center of an element (assumes no degenerate elements)
func (e *Element) Pos() [3]float64 {
	result := *e.Nodes[0]
	for i := 1; i < 8; i++ {
		result = vect3d.Add(result, *e.Nodes[i])
	}
	return vect3d.ScalerMult(result, float64(0.125))
}

// Idx calculates the x,y,z integer index of the element assuming all the elements are the same size. This is used for determining if there is an adjacent element and therefor part of the same fragment.
func (e *Element) Idx() [3]int64 {
	// index of the element (in x, y, and z)
	pos := e.Pos()
	return [3]int64{int64(math.Floor(pos[0] / e.Size)),
		int64(math.Floor(pos[1] / e.Size)), int64(math.Floor(pos[2] / e.Size))}
}

// calcPa cacluates the presented area of one face of an element.
func (e *Element) facePa(id [3]int, velDir [3]float64) float64 {
	node1 := *e.Nodes[id[0]]
	node2 := *e.Nodes[id[1]]
	node3 := *e.Nodes[id[2]]
	A := vect3d.Subtract(node2, node1)
	B := vect3d.Subtract(node3, node2)
	faceNorm := vect3d.CrossProduct(A, B)
	area := vect3d.Magnitude(faceNorm)
	cosAngle := vect3d.DotProduct(faceNorm, velDir) / area
	if cosAngle > 0 {
		return area * cosAngle
	} else {
		return float64(0.0)
	}
}

func (e *Element) Translate(dir [3]float64) {
	for _, n := range e.Nodes {
		n.Translate(dir)
	}
}

func (e *Element) reflectNodeOrder() {
	temp := e.Nodes[0]
	e.Nodes[0] = e.Nodes[1]
	e.Nodes[1] = temp
	temp = e.Nodes[2]
	e.Nodes[2] = e.Nodes[3]
	e.Nodes[3] = temp
	temp = e.Nodes[4]
	e.Nodes[4] = e.Nodes[5]
	e.Nodes[5] = temp
	temp = e.Nodes[6]
	e.Nodes[6] = e.Nodes[7]
	e.Nodes[7] = temp
}

func (e *Element) ReflectX() {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.ReflectX()
	refnode.ReflectX()
	e.Vel = vect3d.Subtract(velnode, refnode)
	e.reflectNodeOrder()
}

func (e *Element) ReflectY() {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.ReflectY()
	refnode.ReflectY()
	e.Vel = vect3d.Subtract(velnode, refnode)
	e.reflectNodeOrder()
}

func (e *Element) ReflectZ() {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.ReflectZ()
	refnode.ReflectZ()
	e.Vel = vect3d.Subtract(velnode, refnode)
	e.reflectNodeOrder()
}

func (e *Element) RotateX(angle float64, center Node) {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.RotateX(angle, center)
	refnode.RotateX(angle, center)
	e.Vel = vect3d.Subtract(velnode, refnode)
}

func (e *Element) RotateY(angle float64, center Node) {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.RotateY(angle, center)
	refnode.RotateY(angle, center)
	e.Vel = vect3d.Subtract(velnode, refnode)
}

func (e *Element) RotateZ(angle float64, center Node) {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.RotateZ(angle, center)
	refnode.RotateZ(angle, center)
	e.Vel = vect3d.Subtract(velnode, refnode)
}

func (e *Element) RotateN1N2(angle float64, center, n1, n2 Node) {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.RotateN1N2(angle, center, n1, n2)
	refnode.RotateN1N2(angle, center, n1, n2)
	e.Vel = vect3d.Subtract(velnode, refnode)
}

func (e *Element) RotateN1N2N3(angle float64, center, n1, n2, n3 Node) {
	refnode := *e.Nodes[0]
	velnode := Node(vect3d.Add(refnode, e.Vel))
	velnode.RotateN1N2N3(angle, center, n1, n2, n3)
	refnode.RotateN1N2N3(angle, center, n1, n2, n3)
	e.Vel = vect3d.Subtract(velnode, refnode)
}
