// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	"bitbucket.org/srosencrantz/go_utility/vect3d"
	"math"
)

//Node stores the location of a point
type Node [3]float64

func (n *Node) Translate(dir [3]float64) {
	*n = vect3d.Add(*n, dir)
}

func (n *Node) ReflectX() {
	n.Translate([3]float64{float64(-2.0) * n[0], 0, 0})
}

func (n *Node) ReflectY() {
	n.Translate([3]float64{0, float64(-2.0) * n[1], 0})
}

func (n *Node) ReflectZ() {
	n.Translate([3]float64{0, 0, float64(-2.0) * n[2]})
}

func (n *Node) RotateX(angle float64, center Node) {
	n.RotateN1N2(angle, center, Node{0.0, 0.0, 0.0}, Node{1.0, 0.0, 0.0})
}

func (n *Node) RotateY(angle float64, center Node) {
	n.RotateN1N2(angle, center, Node{0.0, 0.0, 0.0}, Node{0.0, 1.0, 0.0})
}

func (n *Node) RotateZ(angle float64, center Node) {
	n.RotateN1N2(angle, center, Node{0.0, 0.0, 0.0}, Node{0.0, 0.0, 1.0})
}

func calcT(x0, x1, x2 Node) (t float64) {
	t = -1 * vect3d.DotProduct(vect3d.Subtract(x1, x0), vect3d.Subtract(x2, x1)) / math.Pow(vect3d.Magnitude(vect3d.Subtract(x2, x1)), 2)
	return t
}

func calcP(x1, x2 Node, t float64) (p Node) {
	p = vect3d.Add(x1, vect3d.ScalerMult(vect3d.Subtract(x2, x1), t))
	return p
}

// func (n *Node) RotateN1N2(angle float64, center, n1, n2 Node) {
// 	k := vect3d.Normalize(vect3d.Subtract(n2, n1))
// 	x0 := *n
// 	x1 := center
// 	k2 := Node(vect3d.Add(x1, k))
// 	i := vect3d.Normalize(vect3d.Subtract(x0, calcP(x1, k2, calcT(x0, x1, k2))))
// 	j := vect3d.Normalize(vect3d.CrossProduct(k, i))
// 	i2 := Node(vect3d.Add(x1, i))
// 	j2 := Node(vect3d.Add(x1, j))
// 	it := calcT(x0, x1, i2)
// 	jt := calcT(x0, x1, j2)
// 	kt := calcT(x0, x1, k2)
// 	itnew := (it * math.Cos(angle)) + (jt * -1 * math.Sin(angle))
// 	jtnew := (it * math.Sin(angle)) + (jt * math.Cos(angle))
// 	*n = Node(vect3d.Add(x1, vect3d.ScalerMult(i, itnew)))
// 	*n = Node(vect3d.Add(*n, vect3d.ScalerMult(j, jtnew)))
// 	*n = Node(vect3d.Add(*n, vect3d.ScalerMult(k, kt)))
// 	//	log.Println(" x0 ", x0, " x1 ", x1, " i ", i, " j ", j, " k ", k, " i2 ", i2, " j2 ", j2, " k2 ", k2, " it ", it, " jt ", jt, " kt ", kt, " itnew ", itnew, " jtnew ", jtnew, " *n ", *n)
// }

func (n *Node) RotateN1N2(angle float64, center, n1, n2 Node) {

	//	rad_angle := (angle * math.Pi) / 180.0
	rad_angle := angle
	q := vect3d.Subtract(n2, n1)
	axis_length := vect3d.Magnitude(q)

	q = vect3d.ScalerMult(vect3d.ScalerMult(q, float64(1.0)/axis_length), math.Sin(rad_angle/float64(2.0)))
	q0 := math.Cos(rad_angle / float64(2.0))

	u := vect3d.Subtract(*n, center)

	Q11 := math.Pow(q0, 2.0) + math.Pow(q[0], 2.0) - math.Pow(q[1], 2.0) - math.Pow(q[2], 2.0)
	Q12 := 2.0 * (q[0]*q[1] - q0*q[2])
	Q13 := 2.0 * (q[0]*q[2] + q0*q[1])
	Q21 := 2.0 * (q[1]*q[0] + q0*q[2])
	Q22 := math.Pow(q0, 2.0) - math.Pow(q[0], 2.0) + math.Pow(q[1], 2.0) - math.Pow(q[2], 2.0)
	Q23 := 2.0 * (q[1]*q[2] - q0*q[0])
	Q31 := 2.0 * (q[2]*q[0] - q0*q[1])
	Q32 := 2.0 * (q[2]*q[1] + q0*q[0])
	Q33 := math.Pow(q0, 2.0) - math.Pow(q[0], 2.0) - math.Pow(q[1], 2.0) + math.Pow(q[2], 2.0)
	n[0] = u[0]*Q11 + u[1]*Q12 + u[2]*Q13 + center[0]
	n[1] = u[0]*Q21 + u[1]*Q22 + u[2]*Q23 + center[1]
	n[2] = u[0]*Q31 + u[1]*Q32 + u[2]*Q33 + center[2]

}

func (n *Node) RotateN1N2N3(angle float64, center, n1, n2, n3 Node) {
	a := vect3d.Subtract(n2, n1)
	b := vect3d.Subtract(n3, n2)

	if math.Abs(angle) < 0.00001 {
		return
	}

	plane_vec_angle := math.Abs(vect3d.GetAngle(a, b))

	// todo
	if (plane_vec_angle < 0.00001) || ((plane_vec_angle < 180.00001) && (plane_vec_angle > 179.99999)) {
		temp := vect3d.Add(n1, [3]float64{1.0, 0, 0})
		a = vect3d.Subtract(n2, temp)
		plane_vec_angle = math.Abs(vect3d.GetAngle(a, b))
		if (plane_vec_angle < 0.00001) || ((plane_vec_angle < 180.00001) && (plane_vec_angle > 179.99999)) {
			temp = vect3d.Add(n1, [3]float64{0, 1.0, 0})
			a = vect3d.Subtract(n2, temp)
		}
	}

	axis_of_rot := vect3d.CrossProduct(b, a)

	n.RotateN1N2(angle, center, Node{0, 0, 0}, axis_of_rot)
}
