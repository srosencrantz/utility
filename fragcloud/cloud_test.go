// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	"fmt"
	"testing"
)

func TestNewCloud(t *testing.T) {
	acloud := NewCloud(1.0, -1.0, 1.0, 2.0, 20.0, 3.0, 4.0)

	if acloud.elemSize != 1.0 {
		t.Errorf("acloud.elemSize is %g and should be %g", acloud.elemSize, 1.0)
	}

	if acloud.minVf != 0.001 {
		t.Errorf("acloud.minVf is %g and should be %g", acloud.minVf, 0.001)
	}

	if acloud.minDens != 1.0 {
		t.Errorf("acloud.minDens is %g and should be %g", acloud.minDens, 1.0)
	}

	if acloud.minMass != 2.0 {
		t.Errorf("acloud.minMass is %g and should be %g", acloud.minMass, 2.0)
	}

	if acloud.minKe != 3.0 {
		t.Errorf("acloud.minKe is %g and should be %g", acloud.minKe, 3.0)
	}

	if acloud.minVel != 4.0 {
		t.Errorf("acloud.minVel is %g and should be %g", acloud.minVel, 4.0)
	}
}

func TestCreateFragments(t *testing.T) {

	eList := make([]*Element, 0)
	//                                vf,dens,size, velocity,                  position
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-2.5, -0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-2.5, -0.5, 0.5}))
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-2.5, 0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-2.5, 0.5, 0.5}))

	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-1.5, -0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-1.5, -0.5, 0.5}))
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-1.5, 0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-1.5, 0.5, 0.5}))

	eList = append(eList, NewElement(0.4, 2.0, 1.0, [3]float64{1.0, 0.0, 0.0}, [3]float64{-0.5, -0.5, -0.5}))
	eList = append(eList, NewElement(0.4, 2.0, 1.0, [3]float64{1.0, 0.0, 0.0}, [3]float64{-0.5, -0.5, 0.5}))
	eList = append(eList, NewElement(0.4, 2.0, 1.0, [3]float64{1.0, 0.0, 0.0}, [3]float64{-0.5, 0.5, -0.5}))
	eList = append(eList, NewElement(0.4, 2.0, 1.0, [3]float64{1.0, 0.0, 0.0}, [3]float64{-0.5, 0.5, 0.5}))

	eList = append(eList, NewElement(1.0, 0.25, 1.0, [3]float64{0.0, 0.1, 0.0}, [3]float64{0.5, -0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 0.25, 1.0, [3]float64{0.0, 1.0, 0.0}, [3]float64{0.5, -0.5, 0.5}))
	eList = append(eList, NewElement(1.0, 0.25, 1.0, [3]float64{0.0, 1.0, 0.0}, [3]float64{0.5, 0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 0.25, 1.0, [3]float64{0.0, 1.0, 0.0}, [3]float64{0.5, 0.5, 0.5}))

	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{0.0, 2.0, 0.0}, [3]float64{1.5, -0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{0.0, 2.0, 0.0}, [3]float64{1.5, -0.5, 0.5}))
	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{0.0, 2.0, 0.0}, [3]float64{1.5, 0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{0.0, 2.0, 0.0}, [3]float64{1.5, 0.5, 0.5}))

	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{1.1547, 1.1547, 1.1547}, [3]float64{2.5, -0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{1.1547, 1.1547, 1.1547}, [3]float64{2.5, -0.5, 0.5}))
	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{1.1547, 1.1547, 1.1547}, [3]float64{2.5, 0.5, -0.5}))
	eList = append(eList, NewElement(1.0, 3.0, 1.0, [3]float64{1.1547, 1.1547, 1.1547}, [3]float64{2.5, 0.5, 0.5}))

	size := []float64{1.0, 1.0, 1.0, 1.0}
	vf := []float64{1.0, 1.0, 1.0, 1.0}
	dens := []float64{0.5, 0.5, 0.5, 0.5}
	mass := []float64{1.0, 1.0, 20.0, 1.0}
	massx := []float64{20.0, 20.0, 20.0, 20.0}
	ke := []float64{1.0, 1.0, 1.0, 50.0}
	vel := []float64{1.0, 2.5, 1.0, 1.0}
	numfrags := []int{2, 1, 1, 1}

	for i := 0; i < len(size); i++ {
		acloud := NewCloud(size[i], vf[i], dens[i], mass[i], massx[i], ke[i], vel[i])
		acloud.CreateFragments(eList)

		fmt.Println(acloud.NumFragsString())
		fmt.Println(acloud.MassString())
		fmt.Println(acloud.VelocityString())
		fmt.Println(acloud.KeString())
		fmt.Println(acloud.FragListString())

		for i, f := range acloud.Fragments() {
			fmt.Println("Fragment", i)
			for _, e := range f.Elements() {
				fmt.Println(e.Idx())
			}
		}

		if len(acloud.Fragments()) != numfrags[i] {
			t.Errorf("number of fragments is %g and should be %g", len(acloud.Fragments()), numfrags[i])
		}
	}

}
