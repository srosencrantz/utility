// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	"fmt"
	"testing"
)

func TestNewElement(t *testing.T) {
	e1 := NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{-2.5, -0.5, -0.5})
	e1Idx := [3]int64{-3, -1, -1}
	if e1.Idx() != e1Idx {
		t.Errorf("e1.Idx is %s and should be %s", fmt.Sprint(e1.Idx()), fmt.Sprint(e1Idx))
	}
	e2 := NewElement(1.0, 2.0, 1.0, [3]float64{3.0, 0.0, 0.0}, [3]float64{2.5, 0.5, -0.5})
	e2Idx := [3]int64{2, 0, -1}
	if e2.Idx() != e2Idx {
		t.Errorf("e1.Idx is %s and should be %s", fmt.Sprint(e2.Idx()), fmt.Sprint(e2Idx))
	}

}
