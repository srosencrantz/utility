// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	"bitbucket.org/srosencrantz/go_utility/vect3d"
	"fmt"
	"log"
	"math"
)

// this is multiplied by the elemSize to determine the duplicate node tolerance
const tolMult float64 = 0.1

// intial values for a new cloud object
const defMinVf float64 = 0.001
const defMinDens float64 = 7.6
const defMinMass float64 = 0.05
const defMaxMass float64 = 6.5
const defMinKe float64 = 0.0
const defMinVel float64 = 0.0

// A Cloud stores a list of fragments (and information to create the fragments given a list of elements)
type Cloud struct {
	// Inputs
	elemSize float64 // the size of an element (for amr the highest refined element)
	minVf    float64 // the minimum volume fraction required for an element to be included in a fragment
	minDens  float64 // the minimum density required for an element to be included in a fragment
	minMass  float64 // the minimum mass required for an element to be included in a fragment
	maxMass  float64 // the maximum mass for an element to be included in a fragment
	minKe    float64 // the minimum kinetic energy required for an element to be included in a fragment
	minVel   float64 // the minimum velocity required for an element to be included in a fragment

	// Calculated
	dupTol float64 // tolerance based on elemSize used to determin if nodes in a fragment are duplicates

	//Outputs
	fragments   []*Fragment // the list of fragments in the fragment cloud
	missingMass float64     // the amount of fragment material mass that was not included in fragments because of the minVf, minDens, minMass, minKe, minVel parameters above.
}

// NewCloud creates a new Cloud object. If minVf, minDens, minMass, minKe, or minVel are less than zero then NewCloud will replace the negative numbers with the default values 0.001, 7.6, 2.5, 0.0, 0.0 respectively.
func NewCloud(elemSize, minVf, minDens, minMass, maxMass, minKe, minVel float64) *Cloud {
	c := &Cloud{elemSize: elemSize, minVf: defMinVf, minDens: defMinDens, minMass: defMinMass,
		minKe: defMinKe, minVel: defMinVel, dupTol: (elemSize * tolMult),
		fragments: make([]*Fragment, 0), missingMass: 0.0}
	if minVf >= 0 {
		c.minVf = minVf
	}
	if minDens >= 0 {
		c.minDens = minDens
	}
	if minMass >= 0 {
		c.minMass = minMass
	}
	if maxMass >= 0 {
		c.maxMass = maxMass
	}
	if minKe >= 0 {
		c.minKe = minKe
	}
	if minVel >= 0 {
		c.minVel = minVel
	}
	return c
}

// addIntVec adds 2 [3]int64 vectors
func addIntVec(a, b [3]int64) [3]int64 {
	return [3]int64{a[0] + b[0], a[1] + b[1], a[2] + b[2]}
}

// CreateFragments parses a list of elements and determines the fragments.
func (d *Cloud) CreateFragments(eList []*Element) {

	sides := [6][3]int64{
		[3]int64{1, 0, 0},
		[3]int64{0, 1, 0},
		[3]int64{0, 0, 1},
		[3]int64{-1, 0, 0},
		[3]int64{0, -1, 0},
		[3]int64{0, 0, -1}}

	log.Println("make element map")
	eMap := make(map[[3]int64]*Element)
	for _, e := range eList {
		if e.Dens >= d.minDens && e.Volm >= d.minVf {
			eMap[e.Idx()] = e
		} else {
			d.missingMass += e.Mass
		}
	}

	log.Println("loop through elements")
	for _, e := range eList {
		if _, ok := eMap[e.Idx()]; ok {
			frag := NewFragment(d.dupTol)
			delete(eMap, e.Idx())
			frag.AddElement(e)
			for i := 0; i < len(frag.Elements()); i++ {
				e2 := frag.Elements()[i]
				for j := 0; j < 6; j++ {
					key := addIntVec(e2.Idx(), sides[j])
					if _, ok2 := eMap[key]; ok2 {
						frag.AddElement(eMap[key])
						delete(eMap, key)
					}
				}
			}
			if frag.Mass() < d.minMass ||
				frag.Mass() > d.maxMass ||
				frag.Ke() < d.minKe ||
				vect3d.Magnitude(frag.Vel()) < d.minVel {
				d.missingMass = d.missingMass + frag.Mass()
				log.Println("Fragement discarded, Number of elements = ", len(frag.Elements()), ", Remaining Elements:", len(eMap))
			} else {
				d.fragments = append(d.fragments, frag)
				log.Println("Fragment kept, Number of elements = ", len(frag.Elements()), ", Remaining Elements:", len(eMap))
			}
		}
	}
}

// Fragments returns the list of fragments from the cloud.
func (d *Cloud) Fragments() []*Fragment {
	return d.fragments
}

// MissingMass returns the mass of fragment material that was not included in the fragments due to one of the minimum constraints.
func (d *Cloud) MissingMass() float64 {
	return d.missingMass
}

// NumFragsString returns a string documenting the number of fragments.
func (d *Cloud) NumFragsString() string {
	return fmt.Sprintln("Number of Fragments: ", len(d.fragments))
}

// MassStats returns the minimum, the maximum, the total, and the average fragment mass for the cloud.
func (d *Cloud) MassStats() (min, max, tot, avg float64) {
	tot = float64(0.0)
	min = math.MaxFloat64
	max = -math.MaxFloat64
	for _, f := range d.fragments {
		tot += f.Mass()
		if f.Mass() < min {
			min = f.Mass()
		}
		if f.Mass() > max {
			max = f.Mass()
		}
	}
	avg = tot / float64(len(d.fragments))
	return min, max, tot, avg
}

// MassString returns a string documenting the minimum, maximum, total, and average fragment mass along with the missing mass.
func (d *Cloud) MassString() string {
	output := fmt.Sprint("Missing Mass: ", d.MissingMass(), "\n")
	min, max, tot, avg := d.MassStats()
	output = fmt.Sprint(output, "Total Fragment Mass: ", tot, "\n")
	output = fmt.Sprint(output, "Missing + Total Mass: ", tot+d.MissingMass(), "\n")
	output = fmt.Sprint(output, "Average Fragment Mass: ", avg, "\n")
	output = fmt.Sprint(output, "Min Fragment Mass: ", min, "\n")
	output = fmt.Sprint(output, "Max Fragment Mass: ", max, "\n")
	return output
}

// VelStats returns the minimum, maximum, and average velocity of the fragments in the cloud.
func (d *Cloud) VelStats() (min, max, avg float64) {
	tot := float64(0.0)
	min = math.MaxFloat64
	max = -math.MaxFloat64
	for _, f := range d.fragments {
		speed := vect3d.Magnitude(f.Vel())
		tot += speed
		if speed < min {
			min = speed
		}
		if speed > max {
			max = speed
		}
	}
	avg = tot / float64(len(d.fragments))
	return min, max, avg
}

// VelocityString returns a string documenting the minimum, maximum, and average velocity of the fragments.
func (d *Cloud) VelocityString() string {
	min, max, avg := d.VelStats()
	output := fmt.Sprint("Average Velocity: ", avg, "\n")
	output = fmt.Sprint(output, "Min Velocity: ", min, "\n")
	output = fmt.Sprint(output, "Max Velocity: ", max, "\n")
	return output
}

// KeStats returns the minimum, maximum, and average kinetic energy of the fragments in the cloud.
func (d *Cloud) KeStats() (min, max, avg float64) {
	tot := float64(0.0)
	min = math.MaxFloat64
	max = -math.MaxFloat64
	for _, f := range d.fragments {
		tot += f.Ke()
		if f.Ke() < min {
			min = f.Ke()
		}
		if f.Ke() > max {
			max = f.Ke()
		}
	}
	avg = tot / float64(len(d.fragments))
	return min, max, avg
}

// KeString returns a string documenting the minimum, maximum, and average kinetic energy of the fragments.
func (d *Cloud) KeString() string {
	min, max, avg := d.KeStats()
	output := fmt.Sprint("Average Kinetic Energy: ", avg, "\n")
	output = fmt.Sprint(output, "Min Kinetic Energy: ", min, "\n")
	output = fmt.Sprint(output, "Max Kinetic Energy: ", max, "\n")
	return output
}

// FragListString returns a string with a header line and one line summarizing each fragment
func (d *Cloud) FragListString() string {
	output := GetFragHeader()
	for _, f := range d.fragments {
		output = fmt.Sprint(output, f.GetFragString())
	}
	return output
}

func (d *Cloud) Translate(dir [3]float64) {
	for _, f := range d.fragments {
		f.Translate(dir)
	}
}

func (d *Cloud) ReflectX() {
	for _, f := range d.fragments {
		f.ReflectX()
	}
}

func (d *Cloud) ReflectY() {
	for _, f := range d.fragments {
		f.ReflectY()
	}
}

func (d *Cloud) ReflectZ() {
	for _, f := range d.fragments {
		f.ReflectZ()
	}
}

func (d *Cloud) RotateX(angle float64, center Node) {
	for _, f := range d.fragments {
		f.RotateX(angle, center)
	}
}

func (d *Cloud) RotateY(angle float64, center Node) {
	for _, f := range d.fragments {
		f.RotateY(angle, center)
	}
}

func (d *Cloud) RotateZ(angle float64, center Node) {
	for _, f := range d.fragments {
		f.RotateZ(angle, center)
	}
}

func (d *Cloud) RotateN1N2(angle float64, center, n1, n2 Node) {
	for _, f := range d.fragments {
		f.RotateN1N2(angle, center, n1, n2)
	}
}

func (d *Cloud) RotateN1N2N3(angle float64, center, n1, n2, n3 Node) {
	for _, f := range d.fragments {
		f.RotateN1N2N3(angle, center, n1, n2, n3)
	}
}

func (d *Cloud) Collapse(center, axis [3]float64, length, radius float64) {
	step := math.Min(length, radius) / float64(100)
	maxlength := length / float64(2.0)
	for _, f := range d.fragments {
		//if (axial_pos(center, axis, f.Pos()) <= maxlength) && (rad_pos(center, axis, f.Pos()) <= radius) {
		vnorm := vect3d.Normalize(f.Vel())
		l_last := math.MaxFloat64
		r_last := math.MaxFloat64
		for i := 1; ; i++ {
			transvect := vect3d.ScalerMult(vnorm, float64(i)*step)
			fragpos := vect3d.Subtract(f.Pos(), transvect)
			l := axial_pos(center, axis, fragpos)
			r := rad_pos(center, axis, fragpos)
			if ((l > l_last) || (r > r_last)) && ((l > maxlength) && (r > radius)) {
				log.Println("Collapse - Fragment didn't intersect the warhead, not moved.\n",
					"Closest distance(axial, radial):", l_last, r_last, "\n",
					"   Next distance(axial, radial):", l, r, "\n",
					"             Max(axial, radial):", maxlength, radius)
				break
			}
			if (l <= maxlength) && (r <= radius) {
				f.Translate(vect3d.ScalerMult(transvect, float64(-1.0)))
				log.Println("Collapse - Fragment moved:", transvect, ", Radius:", r, ", Axial:", l)
				break
			}
			l_last = l
			r_last = r
		}
		//}
	}
}

func axial_pos(center, axis, fragpos [3]float64) float64 {
	return math.Abs(vect3d.DotProduct(vect3d.Subtract(fragpos, center), vect3d.Normalize(axis)))
}

func rad_pos(center, axis, fragpos [3]float64) float64 {
	x1 := center
	x2 := vect3d.Add(x1, axis)
	num := vect3d.Magnitude(vect3d.CrossProduct(vect3d.Subtract(fragpos, x1), vect3d.Subtract(fragpos, x2)))
	denom := vect3d.Magnitude(vect3d.Subtract(x2, x1))
	return num / denom
}
