// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

/*
  the frag cloud library is useful for converting euler elements (as from  CTH) containing a particular material to fragment information or  Lagrangian fragments for use in an explicit fea tool (LS-DYNA).

      When using cth for the euler elements the following spyplot commands can be used to create the required element information to be input into this program.

          spy
          PlotTime(1e-4);
          define spyplt_main()
          {
            XLimits(-50,50);
            YLimits(0,50);
            ZLimits(0,17.78);
            DataOutFilter("VOLM+1",1e-4,1);
            DataOut("My_data","VOLM+1","VX","VY","VZ","DENSM+1");
          }
          endspy

      The commands above will create the ascii file for fragments of material "1" at time 1e-4 seconds.
      You will need to adjust the limits commands to match your mesh.
      Do NOT change the order or number of variables in the DataOut command.

  To use the library you start off by creating a cloud with NewCloud and then adding elements to it. The elements will be automatically converted to fragments.
*/

package fragcloud
