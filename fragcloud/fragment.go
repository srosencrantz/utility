// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	"bitbucket.org/srosencrantz/go_utility/vect3d"
	"fmt"
	"math"
)

//Fragment stores information about a fragment.
type Fragment struct {
	vel      [3]float64 // fragment velocity
	pos      [3]float64 // fragment position
	mesh     *Mesh      // The mesh object stores the elements and nodes for the fragment.
	mass     float64    // mass of the fragment
	ke       float64    // kinetic energy
	pa       float64    // presented area of the fragment
	cd       float64    // coefficient of drag
	parmsSet bool       // flag indicates wether calcParms has been run
}

// NewFragment creates a new fragment object and its associatie mesh.
func NewFragment(tol float64) *Fragment {
	return &Fragment{parmsSet: false, mesh: NewMesh(tol)}
}

// AddElement allows an element to be added to a fragment.
func (f *Fragment) AddElement(e *Element) {
	f.mesh.AddElement(e)
	f.mass = f.mass + e.Mass
}

// calcVel calculates the velocity of the fragment by averaging the velocities of the elements.
func (f *Fragment) calcVel() {
	f.vel = [3]float64{0, 0, 0}
	for _, e := range f.mesh.Elements {
		f.vel = vect3d.Add(f.vel, e.Vel)
	}
	f.vel = vect3d.ScalerMult(f.vel, float64(1.0)/float64(len(f.mesh.Elements)))
}

// calcPos calculates the position of the fragment by averaging the position of all the elements.
func (f *Fragment) calcPos() {
	f.pos = [3]float64{0, 0, 0}
	for _, e := range f.mesh.Elements {
		f.pos = vect3d.Add(f.pos, e.Pos())
	}
	f.pos = vect3d.ScalerMult(f.pos, float64(1.0)/float64(len(f.mesh.Elements)))
}

// calcKe calculates the Kinetice Energy of the fragment.
func (f *Fragment) calcKe() {
	vMag := vect3d.Magnitude(f.vel)
	f.ke = 0.5 * f.mass * vMag * vMag
}

// calcCd calculates the coefficient of drag of the fragment.
func (f *Fragment) calcCd() {
	paX := f.calcPresentedArea([3]float64{1.0, 0.0, 0.0})
	paY := f.calcPresentedArea([3]float64{0.0, 1.0, 0.0})
	paZ := f.calcPresentedArea([3]float64{0.0, 0.0, 1.0})

	maxPa := math.Max(paX, paY)
	maxPa = math.Max(maxPa, paZ)

	avgPa := (paX + paY + paZ) / float64(3.0)

	f.cd = float64(1.205)*(maxPa/avgPa) - float64(0.765)
}

// calcPresentedArea calculates the presented area of the fragment.
func (f *Fragment) calcPresentedArea(paDir [3]float64) float64 {
	pa := float64(0.0)
	eMap := make(map[[3]int64]bool)
	for _, e := range f.mesh.Elements {
		eMap[e.Idx()] = false
	}

	for _, e := range f.mesh.Elements {
		neighbor := [3]int64{e.Idx()[0] + int64(1), e.Idx()[1], e.Idx()[2]}
		if _, ok := eMap[neighbor]; !ok {
			//			pa = pa + e.facePa([3]int{1, 3, 7}, paDir)
			pa = pa + e.facePa([3]int{1, 5, 4}, paDir)
		}

		neighbor = [3]int64{e.Idx()[0], e.Idx()[1] + int64(1), e.Idx()[2]}
		if _, ok := eMap[neighbor]; !ok {
			//		pa = pa + e.facePa([3]int{3, 2, 6}, paDir)
			pa = pa + e.facePa([3]int{1, 2, 6}, paDir)
		}

		neighbor = [3]int64{e.Idx()[0], e.Idx()[1], e.Idx()[2] + int64(1)}
		if _, ok := eMap[neighbor]; !ok {
			//			pa = pa + e.facePa([3]int{4, 5, 7}, paDir)
			pa = pa + e.facePa([3]int{4, 5, 6}, paDir)
		}

		neighbor = [3]int64{e.Idx()[0] - int64(1), e.Idx()[1], e.Idx()[2]}
		if _, ok := eMap[neighbor]; !ok {
			//	pa = pa + e.facePa([3]int{0, 4, 6}, paDir)
			pa = pa + e.facePa([3]int{2, 3, 7}, paDir)
		}

		neighbor = [3]int64{e.Idx()[0], e.Idx()[1] - int64(1), e.Idx()[2]}
		if _, ok := eMap[neighbor]; !ok {
			//		pa = pa + e.facePa([3]int{0, 1, 5}, paDir)
			pa = pa + e.facePa([3]int{0, 4, 7}, paDir)
		}

		neighbor = [3]int64{e.Idx()[0], e.Idx()[1], e.Idx()[2] - int64(1)}
		if _, ok := eMap[neighbor]; !ok {
			//			pa = pa + e.facePa([3]int{0, 2, 3}, paDir)
			pa = pa + e.facePa([3]int{0, 3, 2}, paDir)
		}

	}
	return pa
}

// calcParms calculates all of the calculated parameters of the fragment.
func (f *Fragment) calcParms() {
	f.calcVel()
	f.calcPos()
	f.calcKe()
	f.calcCd()
	f.pa = f.calcPresentedArea(vect3d.Normalize(f.vel))
	f.parmsSet = true
}

// Vel returns the velocity of the fragment.
func (f *Fragment) Vel() [3]float64 {
	if !f.parmsSet {
		f.calcParms()
	}
	return f.vel
}

// Pos returns the position of the fragment.
func (f *Fragment) Pos() [3]float64 {
	if !f.parmsSet {
		f.calcParms()
	}
	return f.pos
}

// Ke returns the kinetic energy of the fragment.
func (f *Fragment) Ke() float64 {
	if !f.parmsSet {
		f.calcParms()
	}
	return f.ke
}

// Cd returns the coefficient of drag of the fragment.
func (f *Fragment) Cd() float64 {
	if !f.parmsSet {
		f.calcParms()
	}
	return f.cd
}

// Pa returns the presented area of the fragment.
func (f *Fragment) Pa() float64 {
	if !f.parmsSet {
		f.calcParms()
	}
	return f.pa
}

// Mass returns the mass of the fragment.
func (f *Fragment) Mass() float64 {
	return f.mass
}

// Nodes returns the fragments nodemap.  Along with the element node lists this would allow you to make a finite element model of the fragment.
func (f *Fragment) Nodes(minid int64) (map[*Node](int64), int64) {
	return f.mesh.NodeMap(minid)
}

// Elements returns the elements that make up the fragment.
func (f *Fragment) Elements() []*Element {
	return f.mesh.Elements
}

// GetFragHeader returns a header string that can be used when listing out summary information about the frag using GetFragString().
func GetFragHeader() string {
	return fmt.Sprintf("%16s%16s%16s%16s%16s%16s%10s%10s%16s%16s%16s%16s%16s\n",
		"mass", "Vx", "Vy", "Vz", "Vtotal", "KE", "numElems", "numNodes", "X", "Y",
		"Z", "Cd", "Presented Area")
}

// GetFragString returns a string containing the mass, velocity (x,y,z), speed, kinetic energy, number of elements, number of nodes, position (x,y,z), coefficient of drag, and the presented area for each fragment.
func (f *Fragment) GetFragString() string {
	pos := f.Pos()
	vel := f.Vel()
	nodemap, _ := f.Nodes(1)
	return fmt.Sprintf("%16.10g%16.10g%16.10g%16.10g%16.10g%16.10g%10d%10d%16.10g%16.10g%16.10g%16.10g%16.10g\n",
		f.Mass(), vel[0], vel[1], vel[2], vect3d.Magnitude(vel), f.Ke(),
		len(f.Elements()), len(nodemap), pos[0],
		pos[1], pos[2], f.Cd(), f.Pa())
}

func (f *Fragment) Translate(dir [3]float64) {
	f.mesh.Translate(dir)
	f.parmsSet = false
}

func (f *Fragment) ReflectX() {
	f.mesh.ReflectX()
	f.parmsSet = false
}

func (f *Fragment) ReflectY() {
	f.mesh.ReflectY()
	f.parmsSet = false
}

func (f *Fragment) ReflectZ() {
	f.mesh.ReflectZ()
	f.parmsSet = false
}

func (f *Fragment) RotateX(angle float64, center Node) {
	f.mesh.RotateX(angle, center)
	f.parmsSet = false
}

func (f *Fragment) RotateY(angle float64, center Node) {
	f.mesh.RotateY(angle, center)
	f.parmsSet = false
}

func (f *Fragment) RotateZ(angle float64, center Node) {
	f.mesh.RotateZ(angle, center)
	f.parmsSet = false
}

func (f *Fragment) RotateN1N2(angle float64, center, n1, n2 Node) {
	f.mesh.RotateN1N2(angle, center, n1, n2)
	f.parmsSet = false
}

func (f *Fragment) RotateN1N2N3(angle float64, center, n1, n2, n3 Node) {
	f.mesh.RotateN1N2N3(angle, center, n1, n2, n3)
	f.parmsSet = false
}
