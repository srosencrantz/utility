// Copyright 2014 Stephen Rosencrantz. All rights reserved.
// Use of this source code is governed by a BSD-style
// License that can be found in the LICENSE file.

package fragcloud

import (
	//	"bitbucket.org/srosencrantz/go_utility/vect3d"
	"math"
)

// A NidList contains a list of 8 indexes into the node array of the mesh object. One index for each node of an element.
type NidList [8]int

// A Mesh contains a lists of nodes and (hex) elements as well as a NidList for each element, and the tolerance that is to be used to determine if two nodes are overlapping
type Mesh struct {
	dupTol   float64
	Elements []*Element
	nodeMap  map[[3]int64](*Node)
}

func (m *Mesh) getNodeMapIndex(n *Node) [3]int64 {
	size := m.dupTol * 2
	return [3]int64{int64(math.Floor(n[0] / size)),
		int64(math.Floor(n[1] / size)), int64(math.Floor(n[2] / size))}
}

// NewMesh returns a new Mesh object and sets the duplicate node tolerance.
func NewMesh(tol float64) *Mesh {
	return &Mesh{dupTol: tol, Elements: make([]*Element, 0), nodeMap: make(map[[3]int64](*Node))}
}

// inTol determines whether the distance between node n2 and node n1 is less than or equal to tol.
func (m *Mesh) inTol(n1, n2 *Node) bool {
	// if vect3d.Magnitude(vect3d.Subtract(*n2, *n1)) <= m.dupTol {
	// 	return true
	// }
	if math.Abs(n2[0]-n1[0]) <= m.dupTol && math.Abs(n2[1]-n1[1]) <= m.dupTol && math.Abs(n2[2]-n1[2]) <= m.dupTol {
		return true
	}
	return false
}

func (m *Mesh) NodeMap(minid int64) (map[*Node](int64), int64) {
	nodemap := make(map[*Node](int64))
	i := minid
	// me = mesh element
	for _, me := range m.Elements {
		// men = mesh element node
		for _, men := range me.Nodes {
			if _, ok := nodemap[men]; !ok {
				nodemap[men] = i
				i++
			}
		}
	}
	return nodemap, i
}

// AddElement adds element e to the mesh and adds nodes from the element that aren't already in the mesh. It also creates an NidList for the element and appends it to ElementNids..
func (m *Mesh) AddElement(e *Element) {
	nodelist := make([]*Node, 8)
	// en = element node
	for i, en := range e.Nodes {
		//found := false
		indx := m.getNodeMapIndex(en)
		if _, ok := m.nodeMap[indx]; ok {
			nodelist[i] = m.nodeMap[indx]
		} else {
			nodelist[i] = en
			m.nodeMap[indx] = en
		}
		// me = mesh element
		//for _, me := range m.Elements {
		// men = mesh element node
		//for _, men := range me.Nodes {
		//if m.inTol(men, en) {
		//nodelist[i] = men
		//found = true
		//break
		//}
		//}
		//}
		//if !found {
		//	nodelist[i] = en
		//}
	}
	elem := &Element{e.Volm, e.Dens, e.Size, e.Mass, e.Vol, e.Vel, nodelist}
	m.Elements = append(m.Elements, elem)
}

func (m *Mesh) Translate(dir [3]float64) {
	for _, n := range m.nodeMap {
		n.Translate(dir)
	}
}

func (m *Mesh) ReflectX() {
	for _, e := range m.Elements {
		e.ReflectX()
	}
	for _, n := range m.nodeMap {
		n.ReflectX()
	}
}

func (m *Mesh) ReflectY() {
	for _, e := range m.Elements {
		e.ReflectY()
	}
	for _, n := range m.nodeMap {
		n.ReflectY()
	}
}

func (m *Mesh) ReflectZ() {
	for _, e := range m.Elements {
		e.ReflectZ()
	}
	for _, n := range m.nodeMap {
		n.ReflectZ()
	}
}

func (m *Mesh) RotateX(angle float64, center Node) {
	for _, e := range m.Elements {
		e.RotateX(angle, center)
	}
	for _, n := range m.nodeMap {
		n.RotateX(angle, center)
	}
}

func (m *Mesh) RotateY(angle float64, center Node) {
	for _, e := range m.Elements {
		e.RotateY(angle, center)
	}
	for _, n := range m.nodeMap {
		n.RotateY(angle, center)
	}
}

func (m *Mesh) RotateZ(angle float64, center Node) {
	for _, e := range m.Elements {
		e.RotateZ(angle, center)
	}
	for _, n := range m.nodeMap {
		n.RotateZ(angle, center)
	}
}

func (m *Mesh) RotateN1N2(angle float64, center, n1, n2 Node) {
	for _, e := range m.Elements {
		e.RotateN1N2(angle, center, n1, n2)
	}
	for _, n := range m.nodeMap {
		n.RotateN1N2(angle, center, n1, n2)
	}
}

func (m *Mesh) RotateN1N2N3(angle float64, center, n1, n2, n3 Node) {
	for _, e := range m.Elements {
		e.RotateN1N2N3(angle, center, n1, n2, n3)
	}
	for _, n := range m.nodeMap {
		n.RotateN1N2N3(angle, center, n1, n2, n3)
	}
}

func (m *Mesh) Copy() *Mesh {
	m2 := NewMesh(m.dupTol)
	for _, e := range m.Elements {
		m2.AddElement(NewElement(e.Volm, e.Dens, e.Size, e.Vel, e.Pos()))
	}
	return m2
}

func (m *Mesh) Add(m2 *Mesh) {
	for _, e := range m2.Elements {
		m.AddElement(NewElement(e.Volm, e.Dens, e.Size, e.Vel, e.Pos()))
	}
}
