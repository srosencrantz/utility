package fragcloud

import (
	"bitbucket.org/srosencrantz/go_utility/vect3d"
	"math"
	"testing"
)

func Close(n1, n2 Node, tol float64) bool {
	diff := vect3d.Magnitude(vect3d.Subtract(n1, n2))
	return diff < tol
}

func TestRotateX(t *testing.T) {
	test := Node{10, 9, 8}
	angle := (float64(23) * math.Pi) / float64(180.0)
	center := Node{-1, -2, -3}
	result := Node{10.0, 3.8275108, 11.423595}
	test.RotateX(angle, center)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateX result doesn't match expected, expected:\n%s\nGot:\n%s\n", result, test)
	}

}

func TestRotateXa(t *testing.T) {
	test := Node{0, 1, 0}
	angle := float64(math.Pi / float64(2.0))
	center := Node{0, 0, 0}
	result := Node{0, 0, 1}
	test.RotateX(angle, center)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateX result doesn't match expected, expected:\n%s\nGot:\n%s\n", result, test)
	}

}

func TestRotateY(t *testing.T) {
	test := Node{10, 9, 8}
	angle := (float64(91) * math.Pi) / float64(180.0)
	center := Node{-4, -5, -6}
	result := Node{9.7535343, 9.0, -20.242201}
	test.RotateY(angle, center)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateY result doesn't match expected, expected:\n%s\nGot:\n%s\n", result, test)
	}
}

func TestRotateZ(t *testing.T) {
	test := Node{10, 9, 8}
	angle := (float64(-270) * math.Pi) / float64(180.0)
	center := Node{33, -34, 35}
	result := Node{-9.9999962, -57.000008, 8.0}
	test.RotateZ(angle, center)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateZ result doesn't match expected, expected:\n%s\nGot:\n%s\n", result, test)
	}
}

func TestRotateN1N2(t *testing.T) {
	test := Node{10, 9, 8}
	angle := (float64(333) * math.Pi) / float64(180.0)
	center := Node{1.5, 2.8, 3.2}
	n1 := Node{50, 10, 3}
	n2 := Node{3, 10, 50}
	result := Node{11.265516, 4.0546761, 9.2655163}
	test.RotateN1N2(angle, center, n1, n2)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateN1N2 result doesn't match expected, expected:\n%s\nGot:\n%s\n", result, test)
	}
}

func TestRotateN1N2N3(t *testing.T) {
	test := Node{10, 9, 8}
	angle := (float64(13) * math.Pi) / float64(180.0)
	center := Node{1.2, 3.4, 5.6}
	n1 := Node{10, 9, 8}
	n2 := Node{11, 12, 13}
	n3 := Node{14, 15, 16}
	result := Node{8.8193636, 9.4442215, 10.069080}
	test.RotateN1N2N3(angle, center, n1, n2, n3)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateN1N2N3 result doesn't match expected, expected:\n%s\nGot:\n%s\n", result, test)
	}
}
