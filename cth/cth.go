package cth

import (
	"bitbucket.org/srosencrantz/go_utility/fragcloud"
	"bufio"
	"fmt"
	"log"
	"os"
)

func ReadSpyAsciiFile(filename string, size float64) []*fragcloud.Element {
	var err error
	var f *os.File

	if f, err = os.Open(filename); err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	eList := make([]*fragcloud.Element, 0)
	scanner.Scan()
	for scanner.Scan() {
		var vf, dens, vx, vy, vz, x, y, z float64
		fmt.Sscan(scanner.Text(), &x, &y, &z, &vf, &vx, &vy, &vz, &dens)
		eList = append(eList, fragcloud.NewElement(vf, dens, size, [3]float64{vx, vy, vz}, [3]float64{x, y, z}))
	}
	f.Close()
	return eList
}
